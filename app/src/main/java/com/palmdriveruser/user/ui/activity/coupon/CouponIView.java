package com.palmdriveruser.user.ui.activity.coupon;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.PromoResponse;

public interface CouponIView extends MvpView {
    void onSuccess(PromoResponse object);

    void onError(Throwable e);
}
