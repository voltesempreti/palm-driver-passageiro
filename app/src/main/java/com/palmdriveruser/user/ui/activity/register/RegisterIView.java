package com.palmdriveruser.user.ui.activity.register;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.RegisterResponse;
import com.palmdriveruser.user.data.network.model.SettingsResponse;

public interface RegisterIView extends MvpView {

    void onSuccess(SettingsResponse response);

    void onSuccess(RegisterResponse object);

    void onSuccess(Object object);

    void onSuccessPhoneNumber(Object object);

    void onVerifyPhoneNumberError(Throwable e);

    void onError(Throwable e);

    void onVerifyEmailError(Throwable e);
}
