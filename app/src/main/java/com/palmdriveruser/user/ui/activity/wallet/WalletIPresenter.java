package com.palmdriveruser.user.ui.activity.wallet;

import com.palmdriveruser.user.base.MvpPresenter;

import java.util.HashMap;

public interface WalletIPresenter<V extends WalletIView> extends MvpPresenter<V> {
    void addMoney(HashMap<String, Object> obj);
    void wallet();
}
