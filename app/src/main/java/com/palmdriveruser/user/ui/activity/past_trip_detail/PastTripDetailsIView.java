package com.palmdriveruser.user.ui.activity.past_trip_detail;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.Datum;

import java.util.List;

public interface PastTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> pastTripDetails);

    void onError(Throwable e);
}
