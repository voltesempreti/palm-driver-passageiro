package com.palmdriveruser.user.ui.activity.invite_friend;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.User;

public interface InviteFriendIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
