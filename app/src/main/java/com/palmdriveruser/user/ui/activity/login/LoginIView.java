package com.palmdriveruser.user.ui.activity.login;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.ForgotResponse;
import com.palmdriveruser.user.data.network.model.Token;

public interface LoginIView extends MvpView {
    void onSuccess(Token token);

    void onSuccess(ForgotResponse object);

    void onError(Throwable e);
}
