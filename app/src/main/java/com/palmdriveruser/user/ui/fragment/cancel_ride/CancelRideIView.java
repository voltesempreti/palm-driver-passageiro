package com.palmdriveruser.user.ui.fragment.cancel_ride;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.CancelResponse;

import java.util.List;

public interface CancelRideIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);

    void onSuccess(List<CancelResponse> response);

    void onReasonError(Throwable e);
}
