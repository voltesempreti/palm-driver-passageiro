package com.palmdriveruser.user.ui.activity.card;

import com.palmdriveruser.user.base.MvpPresenter;


public interface CarsIPresenter<V extends CardsIView> extends MvpPresenter<V> {
    void card();
}
