package com.palmdriveruser.user.ui.activity.passbook;

import com.palmdriveruser.user.base.MvpPresenter;

public interface WalletHistoryIPresenter<V extends WalletHistoryIView> extends MvpPresenter<V> {
    void wallet();
}
