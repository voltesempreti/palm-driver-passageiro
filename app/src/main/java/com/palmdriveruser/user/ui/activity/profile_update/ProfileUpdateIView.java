package com.palmdriveruser.user.ui.activity.profile_update;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.User;

public interface ProfileUpdateIView extends MvpView {

    void onSuccess(User user);

    void onUpdateSuccess(User user);

    void onError(Throwable e);

    void onSuccessPhoneNumber(Object object);

    void onVerifyPhoneNumberError(Throwable e);

}
