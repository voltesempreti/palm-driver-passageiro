package com.palmdriveruser.user.ui.fragment.searching;

import com.palmdriveruser.user.base.MvpView;

public interface SearchingIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);
}
