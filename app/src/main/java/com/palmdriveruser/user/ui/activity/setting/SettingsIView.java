package com.palmdriveruser.user.ui.activity.setting;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.AddressResponse;

public interface SettingsIView extends MvpView {

    void onSuccessAddress(Object object);

    void onLanguageChanged(Object object);

    void onSuccess(AddressResponse address);

    void onError(Throwable e);
}
