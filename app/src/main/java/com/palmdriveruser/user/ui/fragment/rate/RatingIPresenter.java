package com.palmdriveruser.user.ui.fragment.rate;

import com.palmdriveruser.user.base.MvpPresenter;

import java.util.HashMap;

public interface RatingIPresenter<V extends RatingIView> extends MvpPresenter<V> {

    void rate(HashMap<String, Object> obj);
}
