package com.palmdriveruser.user.ui.fragment.dispute;

public interface DisputeCallBack {
    void onDisputeCreated();
}
