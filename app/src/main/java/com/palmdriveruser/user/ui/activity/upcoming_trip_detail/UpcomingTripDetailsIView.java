package com.palmdriveruser.user.ui.activity.upcoming_trip_detail;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.Datum;

import java.util.List;

public interface UpcomingTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> upcomingTripDetails);

    void onError(Throwable e);
}
