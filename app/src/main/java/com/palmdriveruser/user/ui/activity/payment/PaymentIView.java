package com.palmdriveruser.user.ui.activity.payment;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.Card;

import java.util.List;

public interface PaymentIView extends MvpView {

    void onSuccess(Object card);

    void onSuccess(List<Card> cards);

    void onAddCardSuccess(Object cards);

    void onError(Throwable e);

}
