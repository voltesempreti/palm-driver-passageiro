package com.palmdriveruser.user.ui.activity.passbook;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.WalletResponse;

public interface WalletHistoryIView extends MvpView {
    void onSuccess(WalletResponse response);

    void onError(Throwable e);

}
