package com.palmdriveruser.user.ui.activity.notification_manager;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> notificationManager);

    void onError(Throwable e);

}