package com.palmdriveruser.user.ui.activity.profile;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.User;

public interface ProfileIView extends MvpView {

    void onSuccess(User user);

    void onError(Throwable e);

}
