package com.palmdriveruser.user.ui.activity.wallet;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.AddWallet;
import com.palmdriveruser.user.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {
    void onSuccess(AddWallet object);
    void onError(Throwable e);
    void onSuccess(WalletResponse response);
}
