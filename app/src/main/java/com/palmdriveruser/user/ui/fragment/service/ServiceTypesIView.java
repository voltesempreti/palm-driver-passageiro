package com.palmdriveruser.user.ui.fragment.service;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.Service;

import java.util.List;

public interface ServiceTypesIView extends MvpView {

    void onSuccess(List<Service> serviceList);

    void onError(Throwable e);

    void onSuccess(Object object);
}
