package com.palmdriveruser.user.ui.fragment.service_flow;

import com.palmdriveruser.user.base.MvpPresenter;

public interface ServiceFlowIPresenter<V extends ServiceFlowIView> extends MvpPresenter<V> {

}
