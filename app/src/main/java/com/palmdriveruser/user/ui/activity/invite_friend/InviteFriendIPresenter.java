package com.palmdriveruser.user.ui.activity.invite_friend;

import com.palmdriveruser.user.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
