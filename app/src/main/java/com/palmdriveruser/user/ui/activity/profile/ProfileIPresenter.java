package com.palmdriveruser.user.ui.activity.profile;

import com.palmdriveruser.user.base.MvpPresenter;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface ProfileIPresenter<V extends ProfileIView> extends MvpPresenter<V> {
    void profile();
}
