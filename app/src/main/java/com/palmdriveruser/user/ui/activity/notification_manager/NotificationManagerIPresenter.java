package com.palmdriveruser.user.ui.activity.notification_manager;

import com.palmdriveruser.user.base.MvpPresenter;

public interface NotificationManagerIPresenter<V extends NotificationManagerIView> extends MvpPresenter<V> {
    void getNotificationManager();
}
