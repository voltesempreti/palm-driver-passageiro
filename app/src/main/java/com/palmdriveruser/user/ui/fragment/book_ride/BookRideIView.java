package com.palmdriveruser.user.ui.fragment.book_ride;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.PromoResponse;


public interface BookRideIView extends MvpView {
    void onSuccess(Object object);

    void onError(Throwable e);

    void onSuccessCoupon(PromoResponse promoResponse);
}
