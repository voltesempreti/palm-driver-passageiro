package com.palmdriveruser.user.ui.activity.help;

import com.palmdriveruser.user.base.MvpView;
import com.palmdriveruser.user.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help help);

    void onError(Throwable e);
}
