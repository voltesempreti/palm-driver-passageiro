package com.palmdriveruser.user.ui.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.palmdriveruser.user.MvpApplication;
import com.palmdriveruser.user.R;
import com.palmdriveruser.user.common.Constants;
import com.palmdriveruser.user.data.network.model.Wallet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.MyViewHolder> {

    private List<Wallet> mWallets;
    private Context mContext;

    public WalletAdapter(List<Wallet> wallets) {
        this.mWallets = wallets;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_wallet, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvId.setText(mWallets.get(position).getTransactionAlias());
        holder.tvDate.setText(parseDateToddMMyyyy(mWallets.get(position).getCreatedAt()));
        holder.tvAmt.setText(String.format("%s %s",
                Constants.Currency,
                MvpApplication.getInstance().getNewNumberFormat(mWallets.get(position).getAmount())));

        String stringValue = holder.tvAmt.getText().toString().replaceAll("R", "").replaceAll("\\$", "").replaceAll("\\s+", "").replaceAll(",", ".");
        double value = Double.parseDouble(stringValue);

        Log.i("TestWallet", stringValue);

        if (value > 0) {
            holder.tvAmt.setTextColor(ContextCompat.getColor(mContext, R.color.green));
            holder.tvAmt.setText("R$"+ String.format("%s%s",
                    Constants.Currency,
                    MvpApplication.getInstance().getNewNumberFormat(mWallets.get(position).getAmount())));
            holder.iconTypeAmount.setRotation(180);
            holder.iconTypeAmount.setBackgroundResource(R.drawable.round_green);
        } else if(value < 0){
            holder.iconTypeAmount.setRotation(0);
            holder.tvAmt.setTextColor(ContextCompat.getColor(mContext, R.color.red));
            holder.iconTypeAmount.setBackgroundResource(R.drawable.round_red);
            holder.tvAmt.setText("R$"+ String.format("%s%s",
                    Constants.Currency,
                    MvpApplication.getInstance().getNewNumberFormat(mWallets.get(position).getAmount())));
        }
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public int getItemCount() {
        return mWallets.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvId, tvDate, tvAmt;
        private CardView cvListWallet;
        private ImageView iconTypeAmount;

        private MyViewHolder(View view) {
            super(view);
            tvId = view.findViewById(R.id.tvId);
            tvDate = view.findViewById(R.id.tvDate);
            tvAmt = view.findViewById(R.id.tvAmt);
            cvListWallet = view.findViewById(R.id.cvListWallet);
            iconTypeAmount = view.findViewById(R.id.iconTypeAmount);

            /*

            cvListWallet.setOnClickListener(v -> {
                ArrayList<Wallet> transactionList = new ArrayList<>(mWallets.get(getAdapterPosition()).getTransactions());
                Intent intent = new Intent(mContext, WalletDetailActivity.class);
                intent.putExtra("transaction_list", transactionList);
                intent.putExtra("alias", mWallets.get(getAdapterPosition()).getTransactionAlias());
                mContext.startActivity(intent);
            });

             */
        }
    }
}