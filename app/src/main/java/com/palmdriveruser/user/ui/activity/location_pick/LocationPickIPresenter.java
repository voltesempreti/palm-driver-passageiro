package com.palmdriveruser.user.ui.activity.location_pick;

import com.palmdriveruser.user.base.MvpPresenter;

public interface LocationPickIPresenter<V extends LocationPickIView> extends MvpPresenter<V> {
    void address();
}
