package com.palmdriveruser.user.ui.activity.coupon;

import com.palmdriveruser.user.base.MvpPresenter;

public interface CouponIPresenter<V extends CouponIView> extends MvpPresenter<V> {
    void coupon();
}
