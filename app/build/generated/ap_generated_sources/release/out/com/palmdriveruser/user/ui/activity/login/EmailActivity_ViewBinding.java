// Generated code from Butter Knife. Do not modify!
package com.palmdriveruser.user.ui.activity.login;

import android.view.View;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.appcompat.widget.Toolbar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdriveruser.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EmailActivity_ViewBinding implements Unbinder {
  private EmailActivity target;

  private View view7f0a0278;

  private View view7f0a01de;

  @UiThread
  public EmailActivity_ViewBinding(EmailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public EmailActivity_ViewBinding(final EmailActivity target, View source) {
    this.target = target;

    View view;
    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", EditText.class);
    target.mToolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'mToolbar'", Toolbar.class);
    view = Utils.findRequiredView(source, R.id.sign_up, "method 'onViewClicked'");
    view7f0a0278 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.next, "method 'onViewClicked'");
    view7f0a01de = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    EmailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.email = null;
    target.mToolbar = null;

    view7f0a0278.setOnClickListener(null);
    view7f0a0278 = null;
    view7f0a01de.setOnClickListener(null);
    view7f0a01de = null;
  }
}
