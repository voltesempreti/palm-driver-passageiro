// Generated code from Butter Knife. Do not modify!
package com.palmdriveruser.user.ui.activity.coupon;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.palmdriveruser.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CouponActivity_ViewBinding implements Unbinder {
  private CouponActivity target;

  @UiThread
  public CouponActivity_ViewBinding(CouponActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CouponActivity_ViewBinding(CouponActivity target, View source) {
    this.target = target;

    target.rvCoupon = Utils.findRequiredViewAsType(source, R.id.rvCoupon, "field 'rvCoupon'", RecyclerView.class);
    target.tvNoData = Utils.findRequiredViewAsType(source, R.id.tvNoData, "field 'tvNoData'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CouponActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvCoupon = null;
    target.tvNoData = null;
  }
}
