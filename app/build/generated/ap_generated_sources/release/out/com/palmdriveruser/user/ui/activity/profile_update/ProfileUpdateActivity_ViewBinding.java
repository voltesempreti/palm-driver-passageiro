// Generated code from Butter Knife. Do not modify!
package com.palmdriveruser.user.ui.activity.profile_update;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.palmdriveruser.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ProfileUpdateActivity_ViewBinding implements Unbinder {
  private ProfileUpdateActivity target;

  private View view7f0a0206;

  private View view7f0a024b;

  private View view7f0a0087;

  @UiThread
  public ProfileUpdateActivity_ViewBinding(ProfileUpdateActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ProfileUpdateActivity_ViewBinding(final ProfileUpdateActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.picture, "field 'picture' and method 'onViewClicked'");
    target.picture = Utils.castView(view, R.id.picture, "field 'picture'", CircularImageView.class);
    view7f0a0206 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.completeData = Utils.findRequiredViewAsType(source, R.id.completeData, "field 'completeData'", LinearLayout.class);
    target.firstName = Utils.findRequiredViewAsType(source, R.id.first_name, "field 'firstName'", EditText.class);
    target.lastName = Utils.findRequiredViewAsType(source, R.id.last_name, "field 'lastName'", EditText.class);
    target.cpf = Utils.findRequiredViewAsType(source, R.id.cpf, "field 'cpf'", EditText.class);
    target.mobile = Utils.findRequiredViewAsType(source, R.id.mobile, "field 'mobile'", EditText.class);
    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", EditText.class);
    view = Utils.findRequiredView(source, R.id.save, "field 'save' and method 'onViewClicked'");
    target.save = Utils.castView(view, R.id.save, "field 'save'", Button.class);
    view7f0a024b = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.change_password, "field 'changePassword' and method 'onViewClicked'");
    target.changePassword = Utils.castView(view, R.id.change_password, "field 'changePassword'", TextView.class);
    view7f0a0087 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ProfileUpdateActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.picture = null;
    target.completeData = null;
    target.firstName = null;
    target.lastName = null;
    target.cpf = null;
    target.mobile = null;
    target.email = null;
    target.save = null;
    target.changePassword = null;

    view7f0a0206.setOnClickListener(null);
    view7f0a0206 = null;
    view7f0a024b.setOnClickListener(null);
    view7f0a024b = null;
    view7f0a0087.setOnClickListener(null);
    view7f0a0087 = null;
  }
}
