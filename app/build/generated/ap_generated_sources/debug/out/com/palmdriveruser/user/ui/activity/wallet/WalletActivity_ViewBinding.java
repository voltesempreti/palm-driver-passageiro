// Generated code from Butter Knife. Do not modify!
package com.palmdriveruser.user.ui.activity.wallet;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdriveruser.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class WalletActivity_ViewBinding implements Unbinder {
  private WalletActivity target;

  private View view7f0a0045;

  @UiThread
  public WalletActivity_ViewBinding(WalletActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public WalletActivity_ViewBinding(final WalletActivity target, View source) {
    this.target = target;

    View view;
    target.walletBalance = Utils.findRequiredViewAsType(source, R.id.wallet_balance, "field 'walletBalance'", TextView.class);
    target.tvNoWalletData = Utils.findRequiredViewAsType(source, R.id.tvNoWalletData, "field 'tvNoWalletData'", TextView.class);
    target.amount = Utils.findRequiredViewAsType(source, R.id.amount, "field 'amount'", EditText.class);
    view = Utils.findRequiredView(source, R.id.add_amount, "field 'addAmount' and method 'onViewClicked'");
    target.addAmount = Utils.castView(view, R.id.add_amount, "field 'addAmount'", Button.class);
    view7f0a0045 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    target.rvWalletData = Utils.findRequiredViewAsType(source, R.id.rvWalletData, "field 'rvWalletData'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    WalletActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.walletBalance = null;
    target.tvNoWalletData = null;
    target.amount = null;
    target.addAmount = null;
    target.rvWalletData = null;

    view7f0a0045.setOnClickListener(null);
    view7f0a0045 = null;
  }
}
