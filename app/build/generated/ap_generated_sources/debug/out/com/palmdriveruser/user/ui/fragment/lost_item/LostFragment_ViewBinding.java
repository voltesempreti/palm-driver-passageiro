// Generated code from Butter Knife. Do not modify!
package com.palmdriveruser.user.ui.fragment.lost_item;

import android.view.View;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdriveruser.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LostFragment_ViewBinding implements Unbinder {
  private LostFragment target;

  private View view7f0a00f5;

  private View view7f0a0296;

  @UiThread
  public LostFragment_ViewBinding(final LostFragment target, View source) {
    this.target = target;

    View view;
    target.lost_item_description = Utils.findRequiredViewAsType(source, R.id.lost_item_description, "field 'lost_item_description'", EditText.class);
    view = Utils.findRequiredView(source, R.id.dismiss, "method 'onViewClicked'");
    view7f0a00f5 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.submit, "method 'onViewClicked'");
    view7f0a0296 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LostFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.lost_item_description = null;

    view7f0a00f5.setOnClickListener(null);
    view7f0a00f5 = null;
    view7f0a0296.setOnClickListener(null);
    view7f0a0296 = null;
  }
}
