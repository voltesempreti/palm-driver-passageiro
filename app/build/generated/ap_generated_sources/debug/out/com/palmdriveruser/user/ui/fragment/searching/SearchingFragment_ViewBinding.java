// Generated code from Butter Knife. Do not modify!
package com.palmdriveruser.user.ui.fragment.searching;

import android.view.View;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdriveruser.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SearchingFragment_ViewBinding implements Unbinder {
  private SearchingFragment target;

  private View view7f0a0072;

  @UiThread
  public SearchingFragment_ViewBinding(final SearchingFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.cancel, "method 'onViewClicked'");
    view7f0a0072 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    target = null;


    view7f0a0072.setOnClickListener(null);
    view7f0a0072 = null;
  }
}
