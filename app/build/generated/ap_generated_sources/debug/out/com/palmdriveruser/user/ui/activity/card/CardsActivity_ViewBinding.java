// Generated code from Butter Knife. Do not modify!
package com.palmdriveruser.user.ui.activity.card;

import android.view.View;
import android.widget.Button;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdriveruser.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CardsActivity_ViewBinding implements Unbinder {
  private CardsActivity target;

  private View view7f0a0046;

  private View view7f0a0201;

  @UiThread
  public CardsActivity_ViewBinding(CardsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CardsActivity_ViewBinding(final CardsActivity target, View source) {
    this.target = target;

    View view;
    target.cardsRv = Utils.findRequiredViewAsType(source, R.id.cards_rv, "field 'cardsRv'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.add_card, "field 'addCard' and method 'onViewClicked'");
    target.addCard = Utils.castView(view, R.id.add_card, "field 'addCard'", Button.class);
    view7f0a0046 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    view = Utils.findRequiredView(source, R.id.paypal, "method 'onViewClicked'");
    view7f0a0201 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CardsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cardsRv = null;
    target.addCard = null;

    view7f0a0046.setOnClickListener(null);
    view7f0a0046 = null;
    view7f0a0201.setOnClickListener(null);
    view7f0a0201 = null;
  }
}
