// Generated code from Butter Knife. Do not modify!
package com.palmdriveruser.user.ui.activity.register;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdriveruser.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class RegisterActivity_ViewBinding implements Unbinder {
  private RegisterActivity target;

  private View view7f0a01de;

  private View view7f0a016d;

  @UiThread
  public RegisterActivity_ViewBinding(RegisterActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public RegisterActivity_ViewBinding(final RegisterActivity target, View source) {
    this.target = target;

    View view;
    target.txtReferralCode = Utils.findRequiredViewAsType(source, R.id.txtReferralCode, "field 'txtReferralCode'", EditText.class);
    target.email = Utils.findRequiredViewAsType(source, R.id.email, "field 'email'", EditText.class);
    target.firstName = Utils.findRequiredViewAsType(source, R.id.first_name, "field 'firstName'", EditText.class);
    target.lastName = Utils.findRequiredViewAsType(source, R.id.last_name, "field 'lastName'", EditText.class);
    target.cpf = Utils.findRequiredViewAsType(source, R.id.cpf, "field 'cpf'", EditText.class);
    target.password = Utils.findRequiredViewAsType(source, R.id.password, "field 'password'", EditText.class);
    target.passwordConfirmation = Utils.findRequiredViewAsType(source, R.id.password_confirmation, "field 'passwordConfirmation'", EditText.class);
    target.chkTerms = Utils.findRequiredViewAsType(source, R.id.chkTerms, "field 'chkTerms'", CheckBox.class);
    target.countryImage = Utils.findRequiredViewAsType(source, R.id.countryImage, "field 'countryImage'", ImageView.class);
    target.countryNumber = Utils.findRequiredViewAsType(source, R.id.countryNumber, "field 'countryNumber'", TextView.class);
    target.phoneNumber = Utils.findRequiredViewAsType(source, R.id.phoneNumber, "field 'phoneNumber'", EditText.class);
    target.lnrReferralCode = Utils.findRequiredViewAsType(source, R.id.lnrReferralCode, "field 'lnrReferralCode'", LinearLayout.class);
    view = Utils.findRequiredView(source, R.id.next, "method 'onViewClicked'");
    view7f0a01de = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.lblTerms, "method 'onViewClicked'");
    view7f0a016d = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    RegisterActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.txtReferralCode = null;
    target.email = null;
    target.firstName = null;
    target.lastName = null;
    target.cpf = null;
    target.password = null;
    target.passwordConfirmation = null;
    target.chkTerms = null;
    target.countryImage = null;
    target.countryNumber = null;
    target.phoneNumber = null;
    target.lnrReferralCode = null;

    view7f0a01de.setOnClickListener(null);
    view7f0a01de = null;
    view7f0a016d.setOnClickListener(null);
    view7f0a016d = null;
  }
}
