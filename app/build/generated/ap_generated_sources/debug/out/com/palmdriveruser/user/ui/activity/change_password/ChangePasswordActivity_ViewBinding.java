// Generated code from Butter Knife. Do not modify!
package com.palmdriveruser.user.ui.activity.change_password;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.CallSuper;
import androidx.annotation.UiThread;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.palmdriveruser.user.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ChangePasswordActivity_ViewBinding implements Unbinder {
  private ChangePasswordActivity target;

  private View view7f0a0087;

  @UiThread
  public ChangePasswordActivity_ViewBinding(ChangePasswordActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ChangePasswordActivity_ViewBinding(final ChangePasswordActivity target, View source) {
    this.target = target;

    View view;
    target.oldPassword = Utils.findRequiredViewAsType(source, R.id.old_password, "field 'oldPassword'", EditText.class);
    target.password = Utils.findRequiredViewAsType(source, R.id.password, "field 'password'", EditText.class);
    target.passwordConfirmation = Utils.findRequiredViewAsType(source, R.id.password_confirmation, "field 'passwordConfirmation'", EditText.class);
    view = Utils.findRequiredView(source, R.id.change_password, "field 'changePassword' and method 'onViewClicked'");
    target.changePassword = Utils.castView(view, R.id.change_password, "field 'changePassword'", Button.class);
    view7f0a0087 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    ChangePasswordActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.oldPassword = null;
    target.password = null;
    target.passwordConfirmation = null;
    target.changePassword = null;

    view7f0a0087.setOnClickListener(null);
    view7f0a0087 = null;
  }
}
